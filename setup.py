from setuptools import setup

setup(
    name='check_json',
    version='1.0.1',
    description='Check JSON plugin for nagios/icinga monitoring tools',
    url='https://gitlab.com/Jedimaster0/check_json',
    author='Nathan Snow',
    author_email='admin@mimir-tech.org',
    license='GPLv3',
    packages=['check_json'],
    install_requires=[
        'nagiosplugin',
        'pendulum',
        'flatten_json',
    ],
    zip_safe=False
)